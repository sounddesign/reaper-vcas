# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Used this mimic a digital mixing desk using only a computer and midi controller. Useful for practicing with multitrack recordings.
* It is not advised to use this processing for live performances
* Version 1.1

### How do I get set up? ###

#### Self-contained project ####

* Import / record the audio into Reaper. At the end of the project, create 8 more tracks (or however many VCAs you want to create).
* In "VCA assign.eel" edit the "// Initial values".